/****************************************************************/
/*** simultes particles in a pipe flow	 		      ***/
/*** compile:                                                 ***/
/***        cc -o main main.c -lm -Wall       	      ***/
/*** USAGE: call program without args -> message              ***/
/*** Marcel Kahlen		                              ***/
/*** University of Oldenburg, Germany 2017                    ***/
/****************************************************************/
#ifdef MSDOS
define srand48(s) srand(s)
define drand48() ((double)rand())/(RAND_MAX)
#endif

#define max(a,b) ({ __typeof__ (a) _a = (a); __typeof__ (b) _b = (b); _a > _b ? _a : _b; })

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

/* histogram structure: */
typedef struct{
  int	     numbin;	     /* number of bins */	 
  double     vmin;       /* minimal velocity */
  double     vmax;       /* maximal velocity */
  double     binw;       /* bin width */
  double    *wall;       /* array of v-bin-walls */
  double       *v;       /* array of velocity (in the center of each bin) */
  double     *val;       /* array of values */
} hist_struct;

/* system parameters */
typedef struct{
  int        numb;       /* number of bins */ 
  int        numr;       /* number of runs */ 
  int        numt;       /* number of times */
  double       t0;       /* Start time */ 
  double       tf;       /* End time */   
  double       *t;       /* times */ 
  int          *n;       /* run */ 
} system_struct;

/********************* input() **************************/
/** This function checks the input parameter and reads **/
/** them out.			                       **/
/** PARAMETER:  (*)= return-Parameter                  **/
/**    system:  systemparameter               	       **/
/**      argc:  number of inputparameter               **/
/**      argv:  inputparameter                         **/
/** RETURNS:                                           **/
/**     nothing                                        **/
/********************************************************/
void input(system_struct *system, int argc, char *argv[]){
  int argf = 0, argz = 1, ii;
  
  system->numb = 1000;        
  system->t0 = 0;      
  system->tf = 10;         

  /** select input data **/
  while((argz<argc)&&(argv[argz][0] == '-')){
    if(strcmp(argv[argz], "-numb") == 0)
      sscanf(argv[++argz], "%d", &system->numb), argf++;
    if(strcmp(argv[argz], "-numr") == 0)
      sscanf(argv[++argz], "%d", &system->numr), argf++;
    if(strcmp(argv[argz], "-numt") == 0)
      sscanf(argv[++argz], "%d", &system->numt), argf++;
    if(strcmp(argv[argz], "-t0") == 0)
      sscanf(argv[++argz], "%lf", &system->t0), argf++;
    if(strcmp(argv[argz], "-tf") == 0)
      sscanf(argv[++argz], "%lf", &system->tf), argf++;            
    argz++;
  }	

  /* discription how to give input data */
  if(argf == 0){
    fprintf(stderr, "\nAnleitung: -numr <numr> -numt <numt> <optionen>\n");
    fprintf(stderr, "	   -numb <numb>: number of bins \n");
    fprintf(stderr, "	   -t0 <t0>: start time \n");
    fprintf(stderr, "	   -tf <tf>: end time\n");
    exit(1);
  }
  
  system->t = (double*) malloc(system->numt*sizeof(double)); 
  system->n = (int*) malloc(system->numr*sizeof(int)); 
  
  
  for(ii=0; ii<system->numt; ii++){
    system->t[ii] = (ii+1)*(system->tf-system->t0)/((double) system->numt);
  }      
  for(ii=0; ii<system->numr; ii++){
    system->n[ii] = ii+1;
  }
}

/****************** hist_update() ***********************/
/** counts the rows of the input file                  **/
/** PARAMETERS: (*)= return-parameter                  **/
/**  	 	file: file                                     **/
/** RETURNS:                                           **/
/**     number of rows in the file                     **/ 
/********************************************************/
void hist_update(FILE *file, hist_struct *histogram)
{
  int counter = 0; 
  char puffer[250], str[250];
   
  fscanf(file, "%s", str); fscanf(file, "%s", str); histogram->val[counter] += atoi(str); counter++;
  while(fgets(puffer, 250, file)){
    fscanf(file, "%s", str); fscanf(file, "%s", str); histogram->val[counter] += atoi(str); counter++;
  }  
  rewind(file); /* back to the beginning of the file */ 
}

/***************** hist_setup() *************************/
/** Initializes a histogram               			       **/
/** PARAMETERS: (*)= return-paramter                   **/
/**                                                    **/
/** RETURNS:                                           **/
/**     empty histogram                                **/ 
/********************************************************/
hist_struct hist_setup(system_struct system){
  int ii;
  hist_struct hist;

  hist.vmin = -1; hist.vmax = 1; hist.numbin = system.numb;
  hist.binw = (hist.vmax - hist.vmin)/(hist.numbin);

  hist.wall = (double*) malloc((hist.numbin+1)*sizeof(double)); 
  hist.v = (double*) malloc(hist.numbin*sizeof(double)); 
  hist.val = (double*) malloc(hist.numbin*sizeof(double)); 
  
  for(ii=0; ii<hist.numbin; ii++){
    hist.wall[ii] = hist.vmin + ii*hist.binw;
    hist.v[ii] = hist.vmin + hist.binw/2 + ii*hist.binw; 
    hist.val[ii] = 0; 
  }
  return(hist);
}

int main(int argc, char *argv[]){  
  int ii, jj;
  
  FILE *file, *newfile; 
  char filename[1000];					/* for data saving */ 
  
  system_struct system;         /* system data */
  input(&system, argc, argv);		/* process input data */ 	
  hist_struct histogram;
  
  for(ii=0; ii<system.numt; ii++){
    histogram = hist_setup(system);
    sprintf(filename, "Combined_Results/sim%f.dat", system.t[ii]);
    newfile = fopen(filename, "w");
    for(jj=0; jj<system.numr; jj++){
      sprintf(filename, "Results/hist%f_%d.dat", system.t[ii], system.n[jj]);
      file = fopen(filename, "r");
      
      hist_update(file, &histogram);
      
      fclose(file);
    }
    for(jj=0; jj<system.numb; jj++){
      fprintf(newfile, "%f\t%f\n", histogram.v[jj], histogram.val[jj]);
    }
    fclose(newfile);
  }

  free(histogram.wall); free(histogram.v); free(histogram.val);
  free(system.n); free(system.t);
    
  return(0);
}